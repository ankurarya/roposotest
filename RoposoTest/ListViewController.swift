//
//  ListViewController,swift
//  RoposoTest
//
//  Created by Ankur Arya on 20/03/16.
//  Copyright © 2016 Ankur Arya. All rights reserved.
//

import UIKit
import ObjectMapper

class ListViewController: UIViewController {
    
    //MARK: -----------Outlets-----------------
    
    @IBOutlet weak var tableView: UITableView!
    //MARK: -----------Variables---------------
    
    var storyData = [StoryModel?]()
    var currentStory: StoryModel?
    //MARK: -----------View LifeCycle----------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.intializeView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    //MARK: -----------My Methods---------
    
    func intializeView() {
        
        if let path = NSBundle.mainBundle().pathForResource("iOS-Android Data", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do {
                    let jsonResult: [[String : AnyObject]] = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! [[String : AnyObject]]
                    
                    for dic in jsonResult {
                        let story = Mapper<StoryModel>().map(dic)
                        self.storyData.append(story)
                    }
                    
                    
                    //                    self.storyData = stories
                    self.tableView.reloadData()
                    
                } catch {}
            } catch {}
        }
        
        // setup navigation  title and back image in baseView
    }
    
    //MARK: -----------Actions-----------------
    
    
    //MARK: -----------Notifications-----------
    
    
    //MARK: -----------Others------------------
    
    
    
}

