//
//  StoryModel.swift
//  RoposoTest
//
//  Created by Ankur Arya on 20/03/16.
//  Copyright © 2016 Ankur Arya. All rights reserved.
//

import UIKit
import ObjectMapper

class StoryModel: Mappable {
    
    dynamic var about: String?
    dynamic var storyId: String?
    dynamic var commentCount: NSNumber?
    var likeFlag: Bool?
    dynamic var storyDescription: String?
    dynamic var verb: String?
    dynamic var db: String?
    dynamic var title: String?
    dynamic var type: String?
    dynamic var si: String?
    dynamic var likesCount: NSNumber?
    dynamic var userName: String?
    dynamic var followers: NSNumber?
    dynamic var following: NSNumber?    
    dynamic var image: String?
    dynamic var url: NSNumber?
    dynamic var handle: String?
    var isFollowing: Bool?
    dynamic var createdOn: NSNumber?
    
    
    required init?(_ map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        about                       <- map["about"]
        storyId                     <- map["id"]
        commentCount                <- map["comment_count"]
        likeFlag                    <- map["like_flag"]
        storyDescription            <- map["description"]
        verb                        <- map["verb"]
        db                          <- map["db"]
        title                       <- map["title"]
        type                        <- map["type"]
        si                          <- map["si"]
        likesCount                  <- map["likes_count"]
        userName                    <- map["username"]
        followers                   <- map["followers"]
        following                   <- map["following"]
        image                       <- map["image"]
        url                         <- map["url"]
        handle                      <- map["handle"]
        isFollowing                 <- map["is_following"]
        createdOn                   <- map["createdOn"]
        
    }
    
    init () {
        
    }
    
}
