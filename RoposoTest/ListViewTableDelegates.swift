//
//  ListViewTableDelegates.swift
//  RoposoTest
//
//  Created by Ankur Arya on 20/03/16.
//  Copyright © 2016 Ankur Arya. All rights reserved.
//


import Foundation
import UIKit

extension ListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.storyData.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat{
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCellWithIdentifier("StoryCell") as! StoryCell
        
        let story = self.storyData[indexPath.row]
        if story != nil {
            cell.setCellData(story!)
        }
        cell.selectionStyle = .None
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.currentStory = self.storyData[indexPath.row]
        
        self.performSegueWithIdentifier("goToDetailView", sender: self)
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        if segue!.identifier == "goToDetailView" {
            let viewController:DetailViewController = segue!.destinationViewController as! DetailViewController
            
            viewController.story = self.currentStory
            
        }
        
    }
    
}