//
//  StoryCell.swift
//  RoposoTest
//
//  Created by Ankur Arya on 20/03/16.
//  Copyright © 2016 Ankur Arya. All rights reserved.
//

import UIKit
import KFSwiftImageLoader

class StoryCell: UITableViewCell {
    
    @IBOutlet weak var storyImageView: UIImageView!
    @IBOutlet weak var storyTitle: UILabel!
    @IBOutlet weak var storyDescription: UILabel!
    @IBOutlet weak var followButton: UIButton!
    
    var currentStory : StoryModel?
    
    @IBAction func followClicked(sender: AnyObject) {
        if self.currentStory?.isFollowing == true {
            self.currentStory?.isFollowing = false
            self.followButton.selected = true
        }
        else {
            self.currentStory?.isFollowing = true
            self.followButton.selected = false
        }
    }
    func setCellData(story: StoryModel) {
        self.currentStory = story
        
        if let _ = story.title {
            storyTitle.text = story.title!
        }
        if let _ = story.storyDescription {
            storyDescription.text = story.storyDescription!
        }
        if story.isFollowing != nil {
            followButton.selected = !(story.isFollowing)!
        }
        if  story.si != nil {
            storyImageView.loadImageFromURLString(story.si!)
        }
    }
}
