//
//  DetailViewController.swift
//  RoposoTest
//
//  Created by Ankur Arya on 20/03/16.
//  Copyright © 2016 Ankur Arya. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var followButton: UIButton!
    var story:StoryModel?
    
    //MARK: -----------Outlets-----------------
    
    //MARK: -----------Variables---------------
    
    //MARK: -----------View LifeCycle----------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.intializeView()
    }
    
    override func viewDidDisappear(animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    //MARK: -----------My Methods---------
    
    func intializeView() {
        if self.story?.isFollowing != nil {
            self.followButton.selected = !(self.story?.isFollowing)!
        }
        self.titleLabel.text = self.story?.title
        self.descriptionLabel.text = self.story?.storyDescription
        // setup navigation  title and back image in baseView
    }
    
    //MARK: -----------Actions-----------------
    
    @IBAction func followClicked(sender: AnyObject) {
        if self.story?.isFollowing == true {
            self.story?.isFollowing = false
            self.followButton.selected = false
        }
        else {
            self.story?.isFollowing = true
            self.followButton.selected = true
        }
    }
    
    //MARK: -----------Notifications-----------
    
    
    //MARK: -----------Others------------------
    
}
